/*!
  \author Colin Eckert
  \author Robin Weiss
  \date 09.12.2019
  \brief QT GUI
*/

#ifndef SPANNUNGSTEILERGUI_H
#define SPANNUNGSTEILERGUI_H

#include <QDoubleValidator>
#include <QIntValidator>
#include <QMainWindow>

namespace Ui
{
class SpannungsteilerGUI;
}

class SpannungsteilerGUI : public QMainWindow
{
  Q_OBJECT

 public: 

 /*!
   \brief constructor for QT class 
   \param parent pointer to QT parent class
 */
  explicit SpannungsteilerGUI(QWidget* parent = nullptr);
 /*!
   \brief destructor for QT class
 */
  ~SpannungsteilerGUI(); 

 private:
  Ui::SpannungsteilerGUI* ui;

 private slots:

  void on_ButtonConfirm_clicked(); 
  void on_ButtonReset_clicked();
};

#endif  // SPANNUNGSTEILERGUI_H
