#include <QApplication>
#include <QTextEdit>
#include "SpannungsteilerGUI.h"

int main(int argc, char* argv[])
{
  QApplication a(argc, argv);
  SpannungsteilerGUI w;
  w.show();

  return a.exec();
}
