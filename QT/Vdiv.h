/*!
  \file Vdiv.h
  \author Samuel Niderer
  \author Giovanni Triulzi
  \date 05.12.2019
  \brief Class for resistor calculation
 */

#ifndef VDIV_H_
#define VDIV_H_

#include <math.h>

enum Ereihe {E3, E6, E12, E24};

class Div
{
  public:
    /*!
      \param u input voltage of voltage divider
    */
    void setUi(double u)
    {
      Ui = u;
    }
     /*!
      \param u output voltage of voltage divider
    */
    void setUo(double u)
    {
      Uo = u;
    }
    /*!
     \return value of R1 in \f$\Omega\f$
    */
    
    double getR1()
    {
      return R1;
    }
    /*!
     \return value of R2 in \f$\Omega\f$
    */
    double getR2()
    {
      return R2;
    }
    /*!
     \return real output voltage calculated from R1 and R2
    */
    double getUr()
    {
      // reale Spannung berechnet aus R1 und R2
      return Ui*R2/(R1+R2);
    }
    /*!
     \brief call this function to calculate R1, R2, Ur
    */
    void calc(Ereihe E)
    {
      int arrSizeEx;
      double* exVal;

      switch(E)
        {
          case E3:
            arrSizeEx = arrSizeE3;
            exVal = &e3Val[0];
            break;
          case E6:
            arrSizeEx = arrSizeE6;
            exVal = &e6Val[0];
            break;
          case E12:
            arrSizeEx = arrSizeE12;
            exVal = &e12Val[0];
            break;
          case E24:
            arrSizeEx = arrSizeE24;
            exVal = &e24Val[0];
            break;
        }

      int bestI = 0;
      int bestJ = 0;
      double oldError = 10;

      // k = R1/R2 = (Ui-Uo)/Uo
      double k = (Ui-Uo)/Uo;
      // Spezialfall wenn k=1 -> R1=R2
      if(k == 1)
      {
        // Alle mögliche Kombinationen gleicher Widerstände sind möglich
        bestI = 0;
        bestJ = 0;

      }
      // Fall R1 > R2
      if(k > 1)
      {

        for(int i= (arrSizeEx-1); i > 0; i--)
        {
          // j= i-1, andernfalss vergleich mit j = i, macht kein Sinn
          for(int j= i-1; j >= 0; j--)
          {
            // Berechnung R1/R2
            //eVal[i] = R1, eVal[j] = R2
            double error = fabs(k - (exVal[i] / exVal[j]));

            if(error < oldError)
            {
              bestI = i;
              bestJ = j;
              oldError = error;
            }
          }
        }
      }

      // Fall wenn R1 < R2 ist
      if(k < 1)
      {
        for(int i= 0; i < arrSizeEx - 1; i++)
        {
          for(int j= i+1; j < arrSizeEx; j++)
          {
            // Berechnung R1/R2
            //eVal[i] = R1, eVal[j] = R2
            double error = fabs(k - (exVal[i] / exVal[j]));

            if(error < oldError)
            {
              bestI = i;
              bestJ = j;
              oldError = error;
            }
          }
        }
      }
      R1 = exVal[bestI];
      R2 = exVal[bestJ];
    }

  private:
    enum {arrSizeE3 = 3, arrSizeE6 = 6, arrSizeE12 = 12, arrSizeE24 = 24};

    double Ui;
    double Uo;
    double R1;
    double R2;
    double e3Val[arrSizeE3] = {1, 2.2, 4.7};
    double e6Val[arrSizeE6] = {1, 1.5, 2.2, 3.3, 4.7, 6.8};
    double e12Val[arrSizeE12] = {1, 1.2,1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2};
    double e24Val[arrSizeE24] = {1, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2, 2.2, 2.4, 2.7, 3, 3.3, 3.6, 3.9, 4.3, 4.7, 5.1, 5.6, 6.2, 6.8, 7.5, 8.2, 9.1};
};

#endif /* VDIV_H_ */
