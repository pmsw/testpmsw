/*!
  \author Colin Eckert
  \author Robin Weiss
  \date 09.12.2019
  \brief QT GUI 
*/


#include "SpannungsteilerGUI.h"
#include <math.h>
#include "Vdiv.h"
#include "ui_SpannungsteilerGUI.h"

Div vdiv;

SpannungsteilerGUI::SpannungsteilerGUI(QWidget* parent)
    : QMainWindow(parent), ui(new Ui::SpannungsteilerGUI)
{
  ui->setupUi(this);

  ui->FensterIn->setValidator(new QDoubleValidator(0, 100, 5, this));
  ui->FensterOut->setValidator(new QDoubleValidator(0, 100, 5, this));
}

SpannungsteilerGUI::~SpannungsteilerGUI()
{
  delete ui;
}

void SpannungsteilerGUI::on_ButtonConfirm_clicked()  // \brief Confirm Button
{
  QString U_In = ui->FensterIn->text();  // \brief  Eingangsspannung aus GUI einlesen
  double EingangsSpannung = U_In.toDouble();  // \brief QString to double

  QString U_Out = ui->FensterOut->text();  // Ausgangsspannung aus GUI einlesen
  double AusgangsSpannung = U_Out.toDouble();  // \brief QString to double

  vdiv.setUi(EingangsSpannung);  // \brief Eingangsspannung einlesen
  vdiv.setUo(AusgangsSpannung);  // \brief Ausgangsspannung einlesen

  /* \brief Test, welche E-Reihe ausgewählt wurde */
  if (ui->ButtonE3->isChecked())
  {
    vdiv.calc(E3);
  }
  else if (ui->ButtonE6->isChecked())
  {
    vdiv.calc(E6);
  }
  else if (ui->ButtonE12->isChecked())
  {
    vdiv.calc(E12);
  }
  else if (ui->ButtonE24->isChecked())
  {
    vdiv.calc(E24);
  }

  ui->DisplayR1->setText(QString::number(vdiv.getR1()));  // R1 ausgeben
  ui->DisplayR2->setText(QString::number(vdiv.getR2()));  // R2 ausgeben

  if (EingangsSpannung < AusgangsSpannung)  // Error U_IN < U_OUT
  {
    ui->ErrorLabel->setText(
        "Fehler: Eingangsspannung grösser \nals Ausgangsspannung");
    // emit ui->ButtonReset->clicked(true);
    ui->FensterIn->setText(" ");
    ui->FensterOut->setText(" ");
    ui->DisplayR1->setText(" ");
    ui->DisplayR2->setText(" ");
    ui->ButtonE3->setChecked(true);
  }
  else
  {
    ui->ErrorLabel->setText("");
  }
}

void SpannungsteilerGUI::on_ButtonReset_clicked()  // Resetwerte
{
  ui->FensterIn->setText(" ");
  ui->FensterOut->setText(" ");
  ui->DisplayR1->setText(" ");
  ui->DisplayR2->setText(" ");
  ui->ErrorLabel->setText("");
  ui->ButtonE3->setChecked(true);
}
