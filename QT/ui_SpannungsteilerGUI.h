/********************************************************************************
** Form generated from reading UI file 'SpannungsteilerGUI.ui'
**
** Created by: Qt User Interface Compiler version 5.12.4
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SPANNUNGSTEILERGUI_H
#define UI_SPANNUNGSTEILERGUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SpannungsteilerGUI
{
public:
    QWidget *centralWidget;
    QLineEdit *DisplayR1;
    QRadioButton *ButtonE3;
    QRadioButton *ButtonE12;
    QRadioButton *ButtonE24;
    QRadioButton *ButtonE6;
    QPushButton *ButtonConfirm;
    QPushButton *ButtonReset;
    QLineEdit *DisplayR2;
    QLineEdit *FensterIn;
    QLabel *TextEingang;
    QLabel *TextAusgang;
    QLineEdit *FensterOut;
    QLabel *TextEReihe;
    QLabel *TextR1;
    QLabel *TextR2;
    QLabel *TextTitel;
    QLabel *Bild;
    QLabel *ErrorLabel;
    QMenuBar *menuBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *SpannungsteilerGUI)
    {
        if (SpannungsteilerGUI->objectName().isEmpty())
            SpannungsteilerGUI->setObjectName(QString::fromUtf8("SpannungsteilerGUI"));
        SpannungsteilerGUI->resize(598, 387);
        QFont font;
        font.setPointSize(11);
        SpannungsteilerGUI->setFont(font);
        SpannungsteilerGUI->setAutoFillBackground(false);
        SpannungsteilerGUI->setStyleSheet(QString::fromUtf8("QMainWindow {\n"
"		border: 1px solid gray;\n"
"}"));
        centralWidget = new QWidget(SpannungsteilerGUI);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        DisplayR1 = new QLineEdit(centralWidget);
        DisplayR1->setObjectName(QString::fromUtf8("DisplayR1"));
        DisplayR1->setGeometry(QRect(160, 250, 121, 31));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DisplayR1->sizePolicy().hasHeightForWidth());
        DisplayR1->setSizePolicy(sizePolicy);
        DisplayR1->setTabletTracking(false);
        DisplayR1->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"		border: 1px solid gray;\n"
"\n"
"}"));
        DisplayR1->setReadOnly(true);
        ButtonE3 = new QRadioButton(centralWidget);
        ButtonE3->setObjectName(QString::fromUtf8("ButtonE3"));
        ButtonE3->setGeometry(QRect(260, 90, 112, 23));
        ButtonE3->setChecked(true);
        ButtonE12 = new QRadioButton(centralWidget);
        ButtonE12->setObjectName(QString::fromUtf8("ButtonE12"));
        ButtonE12->setGeometry(QRect(260, 130, 112, 23));
        ButtonE24 = new QRadioButton(centralWidget);
        ButtonE24->setObjectName(QString::fromUtf8("ButtonE24"));
        ButtonE24->setGeometry(QRect(260, 150, 112, 23));
        ButtonE6 = new QRadioButton(centralWidget);
        ButtonE6->setObjectName(QString::fromUtf8("ButtonE6"));
        ButtonE6->setGeometry(QRect(260, 110, 112, 23));
        ButtonConfirm = new QPushButton(centralWidget);
        ButtonConfirm->setObjectName(QString::fromUtf8("ButtonConfirm"));
        ButtonConfirm->setGeometry(QRect(40, 200, 89, 31));
        ButtonConfirm->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"		background-color: #C0C0C0;\n"
"		border: 1px solid gray;\n"
"		padding: 5 pInx;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"		background-color: #C9C9C9;\n"
"		border: 1px solid gray;\n"
"		padding: 5 px;\n"
"}"));
        ButtonReset = new QPushButton(centralWidget);
        ButtonReset->setObjectName(QString::fromUtf8("ButtonReset"));
        ButtonReset->setGeometry(QRect(140, 200, 89, 31));
        ButtonReset->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"		background-color: #C0C0C0;\n"
"		border: 1px solid gray;\n"
"		padding: 5 px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"		background-color: #C9C9C9;\n"
"		border: 1px solid gray;\n"
"		padding: 5 px;\n"
"}"));
        ButtonReset->setCheckable(true);
        DisplayR2 = new QLineEdit(centralWidget);
        DisplayR2->setObjectName(QString::fromUtf8("DisplayR2"));
        DisplayR2->setGeometry(QRect(160, 280, 121, 31));
        sizePolicy.setHeightForWidth(DisplayR2->sizePolicy().hasHeightForWidth());
        DisplayR2->setSizePolicy(sizePolicy);
        DisplayR2->setTabletTracking(false);
        DisplayR2->setStyleSheet(QString::fromUtf8("QLineEdit {\n"
"		border: 1px solid gray;\n"
"\n"
"}"));
        DisplayR2->setReadOnly(true);
        FensterIn = new QLineEdit(centralWidget);
        FensterIn->setObjectName(QString::fromUtf8("FensterIn"));
        FensterIn->setGeometry(QRect(40, 90, 141, 25));
        TextEingang = new QLabel(centralWidget);
        TextEingang->setObjectName(QString::fromUtf8("TextEingang"));
        TextEingang->setGeometry(QRect(40, 70, 171, 17));
        TextAusgang = new QLabel(centralWidget);
        TextAusgang->setObjectName(QString::fromUtf8("TextAusgang"));
        TextAusgang->setGeometry(QRect(40, 130, 171, 17));
        FensterOut = new QLineEdit(centralWidget);
        FensterOut->setObjectName(QString::fromUtf8("FensterOut"));
        FensterOut->setGeometry(QRect(40, 150, 141, 25));
        TextEReihe = new QLabel(centralWidget);
        TextEReihe->setObjectName(QString::fromUtf8("TextEReihe"));
        TextEReihe->setGeometry(QRect(260, 70, 151, 17));
        TextR1 = new QLabel(centralWidget);
        TextR1->setObjectName(QString::fromUtf8("TextR1"));
        TextR1->setGeometry(QRect(40, 260, 121, 17));
        TextR2 = new QLabel(centralWidget);
        TextR2->setObjectName(QString::fromUtf8("TextR2"));
        TextR2->setGeometry(QRect(40, 290, 111, 17));
        TextTitel = new QLabel(centralWidget);
        TextTitel->setObjectName(QString::fromUtf8("TextTitel"));
        TextTitel->setGeometry(QRect(40, 20, 201, 41));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        TextTitel->setFont(font1);
        TextTitel->setStyleSheet(QString::fromUtf8(""));
        TextTitel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        Bild = new QLabel(centralWidget);
        Bild->setObjectName(QString::fromUtf8("Bild"));
        Bild->setGeometry(QRect(320, 110, 221, 211));
        Bild->setPixmap(QPixmap(QString::fromUtf8("../Desktop/181119Spannungsteiler/SP.png")));
        Bild->setScaledContents(true);
        ErrorLabel = new QLabel(centralWidget);
        ErrorLabel->setObjectName(QString::fromUtf8("ErrorLabel"));
        ErrorLabel->setGeometry(QRect(40, 320, 391, 41));
        ErrorLabel->setStyleSheet(QString::fromUtf8("color: red"));
        ErrorLabel->setScaledContents(true);
        SpannungsteilerGUI->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(SpannungsteilerGUI);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 598, 22));
        SpannungsteilerGUI->setMenuBar(menuBar);
        statusBar = new QStatusBar(SpannungsteilerGUI);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        SpannungsteilerGUI->setStatusBar(statusBar);

        retranslateUi(SpannungsteilerGUI);

        QMetaObject::connectSlotsByName(SpannungsteilerGUI);
    } // setupUi

    void retranslateUi(QMainWindow *SpannungsteilerGUI)
    {
        SpannungsteilerGUI->setWindowTitle(QApplication::translate("SpannungsteilerGUI", "SpannungsteilerGUI", nullptr));
        DisplayR1->setText(QString());
        ButtonE3->setText(QApplication::translate("SpannungsteilerGUI", "E3", nullptr));
        ButtonE12->setText(QApplication::translate("SpannungsteilerGUI", "E12", nullptr));
        ButtonE24->setText(QApplication::translate("SpannungsteilerGUI", "E24", nullptr));
        ButtonE6->setText(QApplication::translate("SpannungsteilerGUI", "E6", nullptr));
        ButtonConfirm->setText(QApplication::translate("SpannungsteilerGUI", "Confirm", nullptr));
        ButtonReset->setText(QApplication::translate("SpannungsteilerGUI", "Reset", nullptr));
        DisplayR2->setText(QString());
        TextEingang->setText(QApplication::translate("SpannungsteilerGUI", "Eingangs-Spannung [V]:", nullptr));
        TextAusgang->setText(QApplication::translate("SpannungsteilerGUI", "Ausgangs-Spannung [V]:", nullptr));
        TextEReihe->setText(QApplication::translate("SpannungsteilerGUI", "Gew\303\274nschte E-Reihe:", nullptr));
        TextR1->setText(QApplication::translate("SpannungsteilerGUI", "Widerstand R1:", nullptr));
        TextR2->setText(QApplication::translate("SpannungsteilerGUI", "Widerstand R2:", nullptr));
        TextTitel->setText(QApplication::translate("SpannungsteilerGUI", "Spannungsteiler GUI", nullptr));
        Bild->setText(QString());
        ErrorLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class SpannungsteilerGUI: public Ui_SpannungsteilerGUI {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SPANNUNGSTEILERGUI_H
