#include "../../Vdiv/src/Vdiv.h"
#include <gtest/gtest.h>

TEST(DivDTest, r1equalsr2)
{
  Div d;
  d.setUi(10);
  d.setUo(5);
  d.calc(E3);
  EXPECT_EQ(d.getR1(), d.getR2());
}
